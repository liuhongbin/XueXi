<?php
    function xmlToArray($xml)
    {    
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);        
        return $values;
    }//xml转换成数组

$data = 'theCityName=成都';
$curlobj = curl_init();
curl_setopt($curlobj, CURLOPT_URL, "http://www.webxml.com.cn/WebServices/WeatherWebService.asmx/getWeatherbyCityName");
curl_setopt($curlobj, CURLOPT_HEADER, 0);
curl_setopt($curlobj, CURLOPT_RETURNTRANSFER, 1);	//不直接打印
curl_setopt($curlobj, CURLOPT_POST, 1);	 //post方式

/*全部数据使用HTTP协议中的"POST"操作来发送。要发送文件，在文件名前面加上@前缀并使用完整路径。这个参数可以通过urlencoded后的字符串类似'para1=val1&para2=val2&...'或使用一个以字段名为键值，字段数据为值的数组。如果value是一个数组，Content-Type头将会被设置成multipart/form-data。*/
curl_setopt($curlobj, CURLOPT_POSTFIELDS, $data);

/*一个用来设置HTTP头字段的数组。使用如下的形式的数组进行设置： array('Content-type: text/plain', 'Content-length: 100')*/
curl_setopt($curlobj, CURLOPT_HTTPHEADER, array("application/x-www-form-urlencoded;charset=utf-8","Content-Length: ".strlen($data)));

curl_setopt($curlobj, CURLOPT_USERAGENT, "user-agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36 LBBROWSER");//代理
$rtn = curl_exec($curlobj);
// var_dump($rtn);
if (!curl_errno($curlobj)) {
	# code...
	header("Content-type: text/html; charset=utf-8");
	$rtn = str_replace("arrayofstring", "xml", $rtn);//转换
	$rtn = xmlToArray($rtn);
	var_dump($rtn);
	//print_r($rtn);
}else{
	echo 'Curl error:'.curl_errno($curlobj);
}
curl_close($curlobj);

