<?php
function curl($url,$params = array(),$header = array(),$timeout = 180){
    if(empty($url)) return $url;
 
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL,$url);//请求url地址
 
    curl_setopt($curl, CURLOPT_HTTPHEADER , $header );  //构造IP
     
    if(!empty($params) && count($params) > 0){
        curl_setopt($curl, CURLOPT_POST, true);
         
        //Request Payload格式数据
        if(isset($params['is_json']) && $params['is_json'] === true){
            unset($params['is_json']);
            $params = json_encode($params);
        }else{
            $params = http_build_query($params);
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);        
    }
 
    //curl_setopt($curl, CURLOPT_HEADER, true);//是否返回响应头信息
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);//是否将结果返回
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);//是否重定向
    //curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   // 只信任CA颁布的证书  
    //curl_setopt($curl, CURLOPT_CAINFO, $cacert); // CA根证书（用来验证的网站证书是否是CA颁布）  
    //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 检查证书中是否设置域名，并且是否与提供的主机名匹配  
    // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0); 
    //curl_setopt($curl, CURLOPT_HTTPHEADER, array("Expect: ")); 
    curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $timeout);//用来告诉PHP脚本在成功连接服务器前等待多久（连接成功之后就会开始缓冲输出），这个参数是为了应对目标服务器的过载，下线，或者崩溃等可能状况；
    curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);//用来告诉成功PHP脚本，从服务器接收缓冲完成前需要等待多长时间。如果目标是个巨大的文件，生成内容速度过慢或者链路速度过慢，这个参数就会很有用。
    // 自动设置Referer
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
    //curl_setopt($curl, CURLOPT_COOKIEJAR, "D:\phpStudy\WWW\cj\cooBE66.tmp");// //写入cookie信息
    //setcookie('cookie_jar', $cookie_jar); //保存cookie路径
    $data = curl_exec($curl); //执行    
    curl_close($curl);
    return $data;
}